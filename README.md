# APLICACION PARA CONTROLAR ROBOT

## Para instalar Bundle
    gem install bundler

## Para instalar las gemas
    bundle install

## Para levantar el servidor (puerto 9292)
    rackup

## Para ejecutar las pruebas en capybara
    cucumber

## Para ejecutar las pruebas rspec
    rspec spec
